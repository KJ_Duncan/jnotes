# place this file in your `autoload` directory to see the available commands.
#
# Kakoune 2021, 2.3.1 Configuration, mawww/kakoune/README.asciidoc, viewed 07 October
#               <https://github.com/mawww/kakoune#231-configuration>
#
# assumptions:
#   jnotes-subcommand is an executable on your $PATH.
#   no other user commands conflict with the below command names.
#
#----------------------------------------------------------------------------------------------------#

map global user 'n' -docstring "write to jnotes from kak selection" \
  %{: nop %sh{ jnotes-insert --write "${kak_selection}" }<ret>}

#----------------------------------------------------------------------------------------------------#

define-command -params 2 jnotes-insert %{ nop %sh{
  ( jnotes-insert --tag "$1" --write "$2" 2>&1 & ) 1>/dev/null 2>&1 0</dev/null
}} -docstring "jnotes-insert --tag $1 --write '$2'"

#----------------------------------------------------------------------------------------------------#

define-command -params 2 jnotes-update-tag %{ nop %sh{
   ( jnotes-update --tag "$1" "$2" 2>&1 & ) 1>/dev/null 2>&1 0</dev/null
}} -docstring "jnotes-update row matching <primary-key> with new <tag> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 2 jnotes-update-text %{ nop %sh{
   ( jnotes-update --write "$1" "$2" 2>&1 & ) 1>/dev/null 2>&1 0</dev/null
}} -docstring "jnotes-update using <primary-key> with new <text> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 2 jnotes-update-group %{ nop %sh{
   ( jnotes-update --group "$1" "$2" 2>&1 & ) 1>/dev/null 2>&1 0</dev/null
}} -docstring "jnotes-update tag matching <before> with <after> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 1 jnotes-delete-pk %{ nop %sh{
   ( jnotes-delete --primary-key "$1" 2>&1 & ) 1>/dev/null 2>&1 0</dev/null
}} -docstring "jnotes-delete row matching <primary-key> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 1 jnotes-delete-tag %{ nop %sh{
   ( jnotes-delete --tag "$1" 2>&1 & ) 1>/dev/null 2>&1 0</dev/null
}} -docstring "jnotes-delete rows matching <tag> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command jnotes-select-all %{ evaluate-commands %sh{
   if output=$(mktemp --directory "${TMPDIR:-/tmp}"/kak-fifo.XXXXXXXX)/fifo && mkfifo ${output};
   then ( jnotes-select --all > ${output} 2>&1 & ) 1>/dev/null 2>&1 0</dev/null

       printf "evaluate-commands -try-client '$kak_opt_toolsclient' %%{"
       printf " edit! -fifo ${output} '*fifo*'\n"
       printf " hook -once -always buffer BufCloseFifo .* %%{"
       printf " nop %%sh{"
       printf " rm -r $(dirname ${output})"
       printf " } } }\n"

    else printf "fail \"%s\"" "jnotes-select-all failed on mktemp and mkfifo."
  fi
}} -docstring "jnotes-select all rows from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 1 jnotes-select-tag %{ evaluate-commands %sh{
   if output=$(mktemp --directory "${TMPDIR:-/tmp}"/kak-fifo.XXXXXXXX)/fifo && mkfifo ${output};
   then ( jnotes-select --tag "$1" > ${output} 2>&1 & ) 1>/dev/null 2>&1 0</dev/null

       printf "evaluate-commands -try-client '$kak_opt_toolsclient' %%{"
       printf " edit! -fifo ${output} '*fifo*'\n"
       printf " hook -once -always buffer BufCloseFifo .* %%{"
       printf " nop %%sh{"
       printf " rm -r $(dirname ${output})"
       printf " } } }\n"

    else printf "fail \"%s\"" "jnotes-select-tag failed on mktemp and mkfifo."
  fi
}} -docstring "jnotes-select rows matching <tag> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 1 jnotes-select-date %{ evaluate-commands %sh{
   if output=$(mktemp --directory "${TMPDIR:-/tmp}"/kak-fifo.XXXXXXXX)/fifo && mkfifo ${output};
   then ( jnotes-select --date "$1" > ${output} 2>&1 & ) 1>/dev/null 2>&1 0</dev/null

       printf "evaluate-commands -try-client '$kak_opt_toolsclient' %%{"
       printf " edit! -fifo ${output} '*fifo*'\n"
       printf " hook -once -always buffer BufCloseFifo .* %%{"
       printf " nop %%sh{"
       printf " rm -r $(dirname ${output})"
       printf " } } }\n"

    else printf "fail \"%s\"" "jnotes-select-date failed on mktemp and mkfifo."
  fi
}} -docstring "jnotes-select rows matching date <YYYY-MM-DD> from sqlite database"

#----------------------------------------------------------------------------------------------------#

define-command -params 1 jnotes-select-pk %{ evaluate-commands %sh{
   if output=$(mktemp --directory "${TMPDIR:-/tmp}"/kak-fifo.XXXXXXXX)/fifo && mkfifo ${output};
   then ( jnotes-select --primary-key "$1" > ${output} 2>&1 & ) 1>/dev/null 2>&1 0</dev/null

       printf "evaluate-commands -try-client '$kak_opt_toolsclient' %%{"
       printf " edit! -fifo ${output} '*fifo*'\n"
       printf " hook -once -always buffer BufCloseFifo .* %%{"
       printf " nop %%sh{"
       printf " rm -r $(dirname ${output})"
       printf " } } }\n"

    else printf "fail \"%s\"" "jnotes-select-pk failed on mktemp and mkfifo."
  fi
}} -docstring "jnotes-select row matching <primary-key> from sqlite database"

#----------------------------------------------------------------------------------------------------#

/* User Manual available at https://docs.gradle.org/7.5.1/userguide/building_java_projects.html */
plugins {
  application
  id("org.graalvm.buildtools.native") version "0.9.13"
}

group = "au.kjd.jnotes"
version = "0.2.0"

val moduleName by extra("au.kjd.jnotes")
val javaHome = System.getProperty("java.home")

repositories { mavenCentral() }

dependencies {
  constraints {
    // DO NOT USE VERSION "org.xerial:sqlite-jdbc:3.39.2.1" <-- breaks the GraalVM native-image with a java.lang.NullPointerException
    implementation("org.xerial:sqlite-jdbc:3.39.2.0")
    implementation("info.picocli:picocli:4.6.3")
    annotationProcessor("info.picocli:picocli-codegen:4.6.3") // GraalVM native-image

    testImplementation("org.junit.jupiter:junit-jupiter:5.9.0")
    testImplementation("org.assertj:assertj-core:3.23.1")
  }
  dependencies {
    implementation("org.xerial:sqlite-jdbc")
    implementation("info.picocli:picocli")
    annotationProcessor("info.picocli:picocli-codegen")

    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.assertj:assertj-core")
  }
}

application {
  mainModule.set("au.kjd.jnotes")
  mainClass.set("au.kjd.jnotes.Main")
}

/*
 * If you do this, the plugin will search for 2 environment
 * variables: GRAALVM_HOME and JAVA_HOME in that order. If one of
 * them is set, it will assume that it points to a valid GraalVM
 * installation and completely bypass toolchain selection. Therefore,
 * it becomes your responsibility to make sure that the environment
 * variable points to a JDK that is compatible with your build script
 * requirements (in particular, the language version).
 *
 * GraalVM 2022, 'Disabling toolchain detection', Gradle plugin for GraalVM Native Image building,
 *                version 0.9.13, viewed 29 August 2022,
 *                <https://graalvm.github.io/native-build-tools/latest/gradle-plugin.html#configuration-toolchains-disabling>
 *
 *
 * Running the below command shows that the graalvmNative
 * plugin puts its first positional argument as '-cp'
 * $ ./gradlew jnotes:nativeCompile --info --rerun-tasks
 *
 * A way to overcome this to honour module boundaries is first run:
 * $ ./gradlew compileJava processResources classes jar generateResourcesConfigFile
 *
 * Then the location of the 'sqlite-jdbc-3.39.2.0.jar' & 'picocli-4.6.3.jar' may be in a different sub-directory structure on your machine
 * $ $GRAALVM_HOME/bin/native-image --module-path jnotes/build/libs/jnotes-0.2.0.jar:$HOME/.gradle/caches/modules-2/files-2.1/org.xerial/sqlite-jdbc/3.39.2.0/d8f239a51f8fd75190f6e978a08008d6c53e5e35/sqlite-jdbc-3.39.2.0.jar:$HOME/.gradle/caches/modules-2/files-2.1/info.picocli/picocli/4.6.3/18177f4c3d65cc94e6d4039775ec5aed8089f8d0/picocli-4.6.3.jar \
 *                                  --no-fallback \
 *                                  -H:+BuildOutputColorful \
 *                                  -H:Path=jnotes/build/native/nativeCompile \
 *                                  -H:Name=jnotes \
 *                                  -H:ConfigurationFileDirectories=jnotes/build/native/generated/generateResourcesConfigFile \
 *                                  -H:Module=au.kjd.jnotes \
 *                                  -H:+ReportExceptionStackTraces \
 *                                  --diagnostics-mode
 *
 * You can read the generated reports
 * $ ls -R reports/
 * $ ls -R jnotes/build/native/nativeCompile/reports/
 *
 * Your folder and file auto-generated numbers will be different, but you get the idea.
 * $ cat jnotes/build/native/nativeCompile/reports/diagnostics_20220831_214021/class_initialization_report_20220831_214040.csv | grep RUN_TIME
 *
 * Gradle 2022, 'Executing multiple tasks', Command-Line Interface,
 *               version 7.5.1, viewed 31 August 2022,
 *               <https://docs.gradle.org/current/userguide/command_line_interface.html#executing_multiple_tasks>
 */
graalvmNative {
  toolchainDetection.set(false)

  binaries {
    named("main") {
      imageName.set("jnotes")
      mainClass.set("au.kjd.jnotes.Main")
      buildArgs.add("-H:+ReportExceptionStackTraces")
      // buildArgs.add("--diagnostics-mode")
    }
  }
}

tasks.withType<JavaCompile>() { options.compilerArgs = listOf("--release", "17", "-Aproject=${project.group}/${project.name}") }
tasks.withType<Test>() { jvmArgs("--enable-preview", "-enableassertions") }

tasks {

  compileJava {
    options.javaModuleMainClass.set("au.kjd.jnotes.Main")
    options.javaModuleVersion.set(provider { project.version as String })
    java.modularity.inferModulePath.set(true)
  }

  test {
    useJUnitPlatform { excludeTags("Broken") }
    failFast = true
    testLogging.showStandardStreams = false
    testLogging {
      events(
        "passed",
        "failed",
        "skipped"
      )
    }
  }

  jar {
    enabled = true

    from(sourceSets.main.get().output)
    exclude { details: FileTreeElement ->
      details.file.name.endsWith(".jpg") ||
      details.file.name.endsWith(".png") ||
      details.file.name.endsWith(".puml")
    }

    manifest {
      attributes(
        "Manifest-Version" to "1.0",
        "Created-By" to "Kirk Duncan",
        "Name" to "au/kjd/jnotes/",
        "Sealed" to "true",
        "Main-Class" to "au.kjd.jnotes.Main",
        "Implementation-Title" to "Note-taking command line application to sqlite database",
        "Implementation-Version" to project.version
      )
    }
  }

  register<Exec>("jlink") {
    description = "Build jnotes optimised custom runtime image and optional jar"
    val outputDir by extra("$buildDir/jrt-jnotes")
    inputs.files(configurations.runtimeClasspath)
    inputs.files(jar)
    outputs.dir(outputDir)
    doFirst {
      val modulePath = files(jar) + configurations.runtimeClasspath.get()
      logger.lifecycle(modulePath.joinToString("\n", "jlink module path:\n"))
      logger.lifecycle("\njlink custom runtime:\n ${outputDir}\n")
      delete(outputDir)
      commandLine("$javaHome/bin/jlink",
        "--no-man-pages",
        "--strip-debug",
        "--compress", "2",
        "--module-path",
        listOf("$javaHome/jmods/", modulePath.asPath).joinToString(File.pathSeparator),
        "--add-modules", moduleName,
        "--output", outputDir
      )
    }
  }
}


module au.kjd.jnotes {
  requires java.base;
  requires java.logging;
  requires java.sql;
  requires info.picocli;

  opens au.kjd.jnotes.cli to info.picocli;
}

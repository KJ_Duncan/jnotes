package au.kjd.jnotes.cli;

import au.kjd.jnotes.data.model.ID;
import au.kjd.jnotes.data.model.TAG;
import au.kjd.jnotes.data.model.TEXT;

import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.List;

import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;

import static au.kjd.jnotes.data.domain.SQLCommands.NotesDML.update;
import static au.kjd.jnotes.data.domain.SubClassTCL.disconnect;
import static au.kjd.jnotes.data.domain.SubClassTCL.init;
import static au.kjd.jnotes.fpinjava.Case.cause;
import static au.kjd.jnotes.fpinjava.Case.match;
import static au.kjd.jnotes.fpinjava.Result.failure;
import static au.kjd.jnotes.fpinjava.Result.success;


/* invokes sql data manipulation language on a sqlite database */
@Command(name = "update", usageHelpAutoWidth = true, description = "sql data manipulation language on an sqlite database.", customSynopsis = {
    "@|bold jnotes update|@ [@|fg(yellow) -h|@] [@|fg(yellow) -t|@=@|italic pk label|@] [@|fg(yellow) -w|@=@|italic pk text|@] [@|fg(yellow) -g|@=@|italic label label|@]"
})
public final class Updates implements Callable<Integer> {

  @ParentCommand
  private Cli parent;

  // --tag pk label
  @ArgGroup(exclusive = true, multiplicity = "0..1")
  UpdatesTag updatesTag;

  static class UpdatesTag {
    @Option(names = { "-t", "--tag" }, arity = "2", paramLabel = "label", description = "update a rows tag by its primary key: --tag 1 label")
    private List<String> uTag;
  }

  // --write pk 'text'
  @ArgGroup(exclusive = true, multiplicity = "0..1")
  UpdatesWrite updatesWrite;

  static class UpdatesWrite {
    @Option(names = { "-w", "--write" }, arity = "2", paramLabel = "text", description = "update a rows text by its primary key: --write 1 \"some text string\"")
    private List<String> uWrite;
  }

  // --group before after
  @ArgGroup(exclusive = true, multiplicity = "0..1")
  UpdatesGroup updatesGroup;

  static class UpdatesGroup {
    @Option(names = { "-g", "--group" }, arity = "2", paramLabel = "label", description = "update the tag attribute rows group by the --group <before> <after>")
    private List<String> uGroup;
  }

  @Option(names = { "-h", "--help" }, usageHelp = true, description = "display this help message")
  private boolean helpRequested;

  /* lazy boolean suppliers for match control structure */
  private final BooleanSupplier path  = () -> null != parent.path;
  private final BooleanSupplier tag   = () -> null != updatesTag && null != updatesTag.uTag && !updatesTag.uTag.isEmpty();
  private final BooleanSupplier write = () -> null != updatesWrite && null != updatesWrite.uWrite && !updatesWrite.uWrite.isEmpty();
  private final BooleanSupplier group = () -> null != updatesGroup && null != updatesGroup.uGroup && !updatesGroup.uGroup.isEmpty();
  private final Supplier<String> fmt  = () -> format("%s", parent.path.toAbsolutePath());

  /* System exit code 0 or 1 */
  private final IntSupplier success = () -> 0;
  private final IntSupplier failure = () -> 1;

  public Updates() {}

  @Override public Integer call() {

    /* check given a path */
    if (!path.getAsBoolean()) return 1;

    try {
      /* create database connection from command line path */
      init(fmt.get());

      /* evaluate user decision and return exit code 0 or 1 */
      return match(
          cause(() -> failure("no data in the domain")),
          cause(tag,   () -> success(update(new ID(parseInt(updatesTag.uTag.get(0))), new TAG(updatesTag.uTag.get(1))))),
          cause(write, () -> success(update(new ID(parseInt(updatesWrite.uWrite.get(0))), new TEXT(updatesWrite.uWrite.get(1))))),
          cause(group, () -> success(update(new TAG(updatesGroup.uGroup.get(0)), new TAG(updatesGroup.uGroup.get(1)))))
      ).bind(success, failure);

    } catch (SQLException sqle) { return sqle.getErrorCode(); }
    finally { disconnect(); }
  }
}

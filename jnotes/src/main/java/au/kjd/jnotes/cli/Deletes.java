package au.kjd.jnotes.cli;

import au.kjd.jnotes.data.model.ID;
import au.kjd.jnotes.data.model.TAG;

import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;

import static java.lang.String.format;

import static au.kjd.jnotes.data.domain.SQLCommands.NotesDML.delete;
import static au.kjd.jnotes.data.domain.SubClassTCL.disconnect;
import static au.kjd.jnotes.data.domain.SubClassTCL.init;
import static au.kjd.jnotes.fpinjava.Case.cause;
import static au.kjd.jnotes.fpinjava.Case.match;
import static au.kjd.jnotes.fpinjava.Result.failure;
import static au.kjd.jnotes.fpinjava.Result.success;


/* invokes sql data manipulation language on a sqlite database */
@Command(name = "delete", description = "sql data manipulation language on an sqlite database.")
public final class Deletes implements Callable<Integer> {

  @ParentCommand
  private Cli parent;

  @ArgGroup(exclusive = true, multiplicity = "0..1")
  DeletesPk deletesPk;

  static class DeletesPk {
    @Option(names = { "-p", "--primary-key" }, paramLabel = "integer", description = "delete row by its primary key")
    private Integer dPk;
  }

  @ArgGroup(exclusive = true, multiplicity = "0..1")
  DeletesTag deletesTag;

  static class DeletesTag {
    @Option(names = { "-t", "--tag" }, paramLabel = "label", description = "delete rows by the tag attribute")
    private String dTag;
  }

  @Option(names = { "-h", "--help" }, usageHelp = true, description = "display this help message")
  private boolean helpRequested;

  /* lazy boolean suppliers for match control structure */
  private final BooleanSupplier path = () -> null != parent.path;
  private final BooleanSupplier pkey = () -> null != deletesPk && null != deletesPk.dPk;
  private final BooleanSupplier tag  = () -> null != deletesTag && null != deletesTag.dTag;
  private final Supplier<String> fmt = () -> format("%s", parent.path.toAbsolutePath());

  /* System exit code 0 or 1 */
  private final IntSupplier success = () -> 0;
  private final IntSupplier failure = () -> 1;

  public Deletes() {}

  @Override public Integer call() {

    /* check given a path */
    if (!path.getAsBoolean()) return 1;

    try {
      /* create database connection from command line path */
      init(fmt.get());

      /* evaluate user decision and return exit code 0 or 1 */
      return match(
          cause(() -> failure("no data in the domain")),
          cause(pkey, () -> success(delete(new ID(deletesPk.dPk)))),
          cause(tag,  () -> success(delete(new TAG(deletesTag.dTag))))
      ).bind(success, failure);

    } catch (SQLException sqle) { return sqle.getErrorCode(); }
    finally { disconnect(); }
  }
}

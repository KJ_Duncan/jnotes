package au.kjd.jnotes.cli;

import java.nio.file.Path;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;


/* command line interface entry point given four
 * subcommands; delete, insert, select, update.
 *
 * Picocli 2021, 17. Subcommands, viewed 04 October 2021,
 *               <https://picocli.info/#_subcommands>
 */
@Command(name = "jnotes", subcommands = {
   Deletes.class,
   Inserts.class,
   Selects.class,
   Updates.class
})
public final class Cli {

  @Option(names = { "-f", "--file" }, paramLabel = "path", required = true, description = "path to local sqlite database")
  Path path;

  @Option(names = { "-h", "--help" }, usageHelp = true, description = "display this help message")
  private boolean helpRequested;
}

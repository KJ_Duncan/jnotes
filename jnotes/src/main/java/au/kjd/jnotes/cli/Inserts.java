package au.kjd.jnotes.cli;

import au.kjd.jnotes.data.model.TAG;
import au.kjd.jnotes.data.model.TEXT;

import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;

import static java.lang.String.format;

import static au.kjd.jnotes.data.domain.SQLCommands.NotesDDL.create;
import static au.kjd.jnotes.data.domain.SQLCommands.NotesDML.insert;
import static au.kjd.jnotes.data.domain.SubClassTCL.disconnect;
import static au.kjd.jnotes.data.domain.SubClassTCL.init;
import static au.kjd.jnotes.fpinjava.Case.cause;
import static au.kjd.jnotes.fpinjava.Case.match;
import static au.kjd.jnotes.fpinjava.Result.failure;
import static au.kjd.jnotes.fpinjava.Result.success;


/* invokes sql data manipulation language on a sqlite database */
@Command(name = "insert", description = "sql data manipulation language on an sqlite database.")
public final class Inserts implements Callable<Integer> {

  @ParentCommand
  private Cli parent;

  @Option(names = { "-t", "--tag" }, paramLabel = "label", description = "insert a tag label to group by into table")
  private String iTag;

  @Option(names = { "-w", "--write" }, paramLabel = "text", required = true, description = "insert written \"text requires quoting\" into table")
  private String iWrite;

  @Option(names = { "-h", "--help" }, usageHelp = true, description = "display this help message")
  private boolean helpRequested;

  /* lazy boolean suppliers for match control structure */
  private final BooleanSupplier path  = () -> null != parent.path;
  private final BooleanSupplier tag   = () -> null != iWrite && null != iTag;
  private final BooleanSupplier write = () -> null != iWrite;
  private final Supplier<String> fmt  = () -> format("%s", parent.path.toAbsolutePath());

  /* System exit code 0 or 1 */
  private final IntSupplier success = () -> 0;
  private final IntSupplier failure = () -> 1;

  public Inserts() {}

  @Override public Integer call() {

    /* check given a path */
    if (!path.getAsBoolean()) return 1;

    try {
      /* create database connection from command line path
       * if database doesn't exist on path, create it.
       */
      init(fmt.get());
      create();

      /* evaluate user decision and return exit code 0 or 1 */
      return match(
          cause(() -> failure("no data in the domain")),
          cause(tag,   () -> success(insert(new TAG(iTag), new TEXT(iWrite)))),
          cause(write, () -> success(insert(new TEXT(iWrite))))
      ).bind(success, failure);

    } catch (SQLException sqle) { return sqle.getErrorCode(); }
    finally { disconnect(); }
  }
}

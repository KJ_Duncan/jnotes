package au.kjd.jnotes.cli;

import au.kjd.jnotes.data.model.DATE;
import au.kjd.jnotes.data.model.ID;
import au.kjd.jnotes.data.model.TAG;

import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParentCommand;

import static java.lang.String.format;
import static java.lang.System.out;
import static java.time.LocalDate.parse;

import static au.kjd.jnotes.data.domain.SQLCommands.NotesDQL.select;
import static au.kjd.jnotes.data.domain.SubClassTCL.disconnect;
import static au.kjd.jnotes.data.domain.SubClassTCL.init;
import static au.kjd.jnotes.fpinjava.Case.cause;
import static au.kjd.jnotes.fpinjava.Case.match;
import static au.kjd.jnotes.fpinjava.Result.failure;
import static au.kjd.jnotes.fpinjava.Result.success;


/* invokes sql data query language on a sqlite database */
@Command(name = "select", description = "sql data query language on an sqlite database.")
public final class Selects implements Callable<Integer> {

  @ParentCommand
  private Cli parent;

  /* groups options are mutually exclusive */

  @ArgGroup(exclusive = true, multiplicity = "0..1")
  SelectAll selectAll;

  static class SelectAll {
    @Option(names = {"-a", "--all"}, description = "select all rows from jnotes")
    boolean sAll = false;
  }

  @ArgGroup(exclusive = true, multiplicity = "0..1")
  SelectPk selectPk;

  static class SelectPk {
    @Option(names = {"-p", "--primary-key"}, paramLabel = "integer", description = "select row by primary key")
    private Integer sPk;
  }

  @ArgGroup(exclusive = true, multiplicity = "0..1")
  SelectTag selectTag;

  static class SelectTag {
    @Option(names = {"-t", "--tag"}, paramLabel = "label", description = "select rows by tag")
    private String sTag;
  }

  @ArgGroup(exclusive = true, multiplicity = "0..1")
  SelectDate selectDate;

  static class SelectDate {
    @Option(names = {"-d", "--date"}, paramLabel = "yyyy-mm-dd", description = "select rows by date")
    private String sDate;
  }

  @Option(names = { "-h", "--help" }, usageHelp = true, description = "display this help message")
  private boolean helpRequested;

  /* lazy boolean suppliers for match control structure */
  private final BooleanSupplier path = () -> null != parent.path;
  private final BooleanSupplier all  = () -> null != selectAll && selectAll.sAll;
  private final BooleanSupplier pkey = () -> null != selectPk && null != selectPk.sPk;
  private final BooleanSupplier tag  = () -> null != selectTag && null != selectTag.sTag;
  private final BooleanSupplier date = () -> null != selectDate && null != selectDate.sDate;
  private final Supplier<String> fmt = () -> format("%s", parent.path.toAbsolutePath());

  public Selects() {}

  @Override public Integer call() {

    /* check given a path */
    if (!path.getAsBoolean()) return 1;

    /* evaluate command line arguments and return
     * a list of data transfer objects or fail.
     */
    try {
      /* create database connection from command line path */
      init(fmt.get());

      /* evaluate command line arguments and select
       * data from the domain, then transfer the data
       * to standard out via the console or fail.
       */
      match(
          cause(() -> failure("no data in the domain")),
          cause(all,  () -> success(select())),
          cause(pkey, () -> success(select(new ID(selectPk.sPk)))),
          cause(tag,  () -> success(select(new TAG(selectTag.sTag)))),
          cause(date, () -> success(select(new DATE(parse(selectDate.sDate)))))
      ).bind()
       .forEach(out::print);

      return 0;

    } catch (SQLException sqle) { return sqle.getErrorCode(); }
    finally { disconnect(); }
  }
}

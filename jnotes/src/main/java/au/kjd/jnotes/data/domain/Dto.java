package au.kjd.jnotes.data.domain;


public sealed interface Dto permits JNotesDTO { }

/* JNotesDTO toString to standard out */
final record JNotesDTO(Integer jnoId, String jnoTag, String jnoDate, String jnoText) implements Dto {
  @Override public String toString() {
    return """
          _______________________________________________________________________________
          %s | %s | %s | %s
        """.formatted(jnoId, jnoTag, jnoDate, jnoText);
  }
}

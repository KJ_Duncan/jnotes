package au.kjd.jnotes.data.domain;

import au.kjd.jnotes.data.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Savepoint;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.List;

import static java.time.LocalDate.now;

import static au.kjd.jnotes.data.domain.JNotes.*;
import static au.kjd.jnotes.data.domain.SubClassTCL.connect;


public abstract class SQLCommands {

  private static String getAttribute(final DataDomain table) {
    if (table instanceof JNotes jno) { return jno.getAttribute(); }
    else throw new IllegalStateException("No data in the domain");
  }

  private sealed interface NotesTCL permits NotesDML, NotesDDL {

    private static Consumer<Connection> commit() {
      return conn -> { try { conn.commit();
                     } catch (SQLException sqle) { throw new RuntimeException(sqle); }
      };
    }

    private static BiConsumer<Connection, Savepoint> rollback() {
      return (conn, save) -> { try { conn.rollback(save); conn.commit();
                             } catch (SQLException sqle) { throw new RuntimeException(sqle); }
      };
    }

    private static Function<Connection, Savepoint> savepoint() {
      return conn -> { try { return conn.setSavepoint();
                     } catch (SQLException sqle) { throw new RuntimeException(sqle); } };
    }
  }

  public static final class NotesDDL extends SQLCommands implements NotesTCL {
    private NotesDDL() {}

    public static void create() throws SQLException {
      final PreparedStatement ps = connect()
                                   .prepareStatement(getAttribute(JNO_CREATE));
      ps.execute();
      NotesTCL.commit().accept(connect());
    }
  }

  public static final class NotesDML extends SQLCommands implements NotesTCL {
    private NotesDML() {}

    public static boolean insert(final TEXT text) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_INSERT));

      final TAG tag = new TAG("default");

      ps.setString(1, tag.group());
      ps.setString(2, now().toString());
      ps.setString(3, text.write());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    public static boolean insert(final TAG tag,
                                final TEXT text) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_INSERT));

      ps.setString(1, tag.group());
      ps.setString(2, now().toString());
      ps.setString(3, text.write());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    public static boolean update(final ID id,
                                 final TAG tag) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_UPDATE_TAG));

      ps.setString(1, tag.group());
      ps.setInt(2, id.pk());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    public static boolean update(final ID id,
                                 final TEXT text) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_UPDATE_TEXT));

      ps.setString(1, text.write());
      ps.setInt(2, id.pk());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    /* set all tags matching before to after */
    public static boolean update(final TAG before,
                                 final TAG after) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_UPDATE_TAGS));

      ps.setString(1, after.group());
      ps.setString(2, before.group());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    public static boolean delete(final ID id) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_DELETE_ID));

      ps.setInt(1, id.pk());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    public static boolean delete(final TAG tag) throws SQLException {

      final PreparedStatement ps = connect()
                                  .prepareStatement(getAttribute(JNO_DELETE_TAG));

      ps.setString(1, tag.group());
      ps.execute();

      return transaction(ps, NotesTCL.savepoint().apply(connect()));
    }

    private static boolean transaction(PreparedStatement ps, Savepoint sp) throws SQLException {
      try { NotesTCL.commit().accept(connect()); return true; }
      catch (Exception ex) { NotesTCL.rollback().accept(connect(), sp); throw new RuntimeException(ex); }
      finally { ps.clearParameters(); }
    }
  }

  public static final class NotesDQL extends SQLCommands {
    private NotesDQL() { }

    public static List<Dto> select() throws SQLException {

      List<Dto> dtos = new ArrayList<Dto>();

      final PreparedStatement ps = connect()
                                   .prepareStatement(getAttribute(JNO_SELECT_ALL));

      return execute(ps, dtos);
    }

    public static List<Dto> select(final ID id) throws SQLException {

      List<Dto> dtos = new ArrayList<Dto>();

      final PreparedStatement ps = connect()
                                   .prepareStatement(getAttribute(JNO_SELECT_ID));

      ps.setInt(1, id.pk());
      return execute(ps, dtos);
    }

    public static List<Dto> select(final TAG tag) throws SQLException {

      List<Dto> dtos = new ArrayList<Dto>();

      final PreparedStatement ps = connect()
                                   .prepareStatement(getAttribute(JNO_SELECT_TAG));

      ps.setString(1, tag.group());
      return execute(ps, dtos);
    }

    public static List<Dto> select(final DATE date) throws SQLException {

      List<Dto> dtos = new ArrayList<Dto>();

      final PreparedStatement ps = connect()
                                   .prepareStatement(getAttribute(JNO_SELECT_DATE));

      ps.setString(1, date.now().toString());
      return execute(ps, dtos);
    }

    private static List<Dto> execute(final PreparedStatement ps,
                                     final List<Dto> dtos) throws SQLException {

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {

      dtos.add(new JNotesDTO(rs.getInt(getAttribute(JNO_ID)),
                             rs.getString(getAttribute(JNO_TAG)),
                             rs.getString(getAttribute(JNO_DATE)),
                             rs.getString(getAttribute(JNO_TEXT))));
      }

      rs.close();
      ps.clearParameters();

      return dtos;
    }
  }
}

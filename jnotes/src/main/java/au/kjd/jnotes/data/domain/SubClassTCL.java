package au.kjd.jnotes.data.domain;


import java.sql.Connection;
import java.sql.SQLException;

import static java.lang.String.format;
import static java.sql.DriverManager.getConnection;

/* final concrete subclass of the SQLCommands hierarchy */
public final class SubClassTCL {

  private static final String JDBC = "jdbc:sqlite:%s";
  private static Connection connection;
  private static String url;

  private SubClassTCL() { }

  public static void init(String path) throws SQLException {
    url = path;
    connection();
  }

  public static Connection connect() throws SQLException {
    /* timeout in seconds to wait for valid connection */
    if (null != connection && !connection.isValid(1)) { disconnect(); connection(); }
    if (null == connection) connection();
    return connection;
  }

  public static void disconnect() {
    assert null != connection;
    try { connection.close(); }
    catch (SQLException ignore) { /* JVM is shutting down */ }
  }

  private static void connection() throws SQLException {
    connection = getConnection(format(JDBC, url));
    connection.setAutoCommit(false);
  }
}

package au.kjd.jnotes.data.model;


/* autoincrement primary key */
public final record ID(int pk) { }

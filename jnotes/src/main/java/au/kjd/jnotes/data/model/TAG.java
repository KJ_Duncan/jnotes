package au.kjd.jnotes.data.model;


/* a group by label given to a note */
public final record TAG(String group) { }

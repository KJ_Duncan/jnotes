package au.kjd.jnotes.data.domain;


sealed interface DataDomain permits JNotes { }

// ------------------------------------------------------------------------- \\
/* jnotes database schema */
enum JNotes implements DataDomain { // ordinal value 0,1,2,3,4,5,6
  JNO_DROP("DROP TABLE IF EXISTS jnotes"),
  JNO_CREATE("""
      CREATE TABLE IF NOT EXISTS jnotes (
        jno_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        jno_tag  TEXT NOT NULL,
        jno_date TEXT NOT NULL,
        jno_text TEXT NOT NULL,

        CONSTRAINT jno_ck1 CHECK ( 0 < LENGTH( jno_tag  ) ),
        CONSTRAINT jno_ck2 CHECK ( 0 < LENGTH( jno_text ) )
      );
      """),

  JNO_INSERT("INSERT INTO jnotes ( jno_tag, jno_date, jno_text ) VALUES ( ?,?,? )"),

  JNO_UPDATE_TAG("UPDATE OR FAIL jnotes SET jno_tag = ? WHERE jno_id = ?"),
  JNO_UPDATE_TAGS("UPDATE OR FAIL jnotes SET jno_tag = ? WHERE jno_tag = ?"),
  JNO_UPDATE_TEXT("UPDATE OR FAIL jnotes SET jno_text = ? WHERE jno_id = ?"),

  JNO_DELETE_ID("DELETE FROM jnotes WHERE jno_id = ?"),
  JNO_DELETE_TAG("DELETE FROM jnotes WHERE jno_tag = ?"),

  JNO_SELECT_ALL("SELECT * FROM jnotes"),
  JNO_SELECT_ID("SELECT jno_id, jno_tag, jno_date, jno_text FROM jnotes WHERE jno_id = ?"),
  JNO_SELECT_TAG("SELECT jno_id, jno_tag, jno_date, jno_text FROM jnotes WHERE jno_tag = ?"),
  JNO_SELECT_DATE("SELECT jno_id, jno_tag, jno_date, jno_text FROM jnotes WHERE jno_date = ?"),

  JNO_ID("jno_id"),
  JNO_TAG("jno_tag"),
  JNO_DATE("jno_date"),
  JNO_TEXT("jno_text");

  private final String attribute;
  JNotes(String attr) { this.attribute = attr; }
  String getAttribute() { return attribute; }
}

package au.kjd.jnotes.data.model;


/* 2021-10-04 : YYYY-MM-DD */
public final record DATE(java.time.LocalDate now) { }

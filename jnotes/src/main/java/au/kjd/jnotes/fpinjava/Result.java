package au.kjd.jnotes.fpinjava;

import java.util.function.IntSupplier;
import java.sql.SQLException;

/* Wrap our results in a Success or Failure data structure, bind a
 * function to the object that returns an int (System exit code 0 or 1)
 * in the cli package.
 * source adapted:
 *   Saumont, P-Y 2017, Listing 3.6 A Result that can handle Effects, Abstracting control structures,
 *                      Functional Programming in Java, chpt 3, p.65, Manning Publications,
 *                      viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java>
 */
public abstract class Result<T> {

  public abstract T bind();
  public abstract int bind(IntSupplier success, IntSupplier failure);

  private static final class Success<T> extends Result<T> {

    private final T value;

    private Success(T value) { this.value = value; }

    @Override public T bind() { return value; }

    @Override public int bind(IntSupplier success, IntSupplier failure) {  return success.getAsInt(); }
  }

  private static final class Failure<T> extends Result<T> {

    private final RuntimeException error;

    private Failure(String message) { this.error = new IllegalStateException(message); }

    private Failure(SQLException sqlex) { this.error = new IllegalStateException(sqlex.getMessage(), sqlex); }

    @SuppressWarnings("unchecked")
    @Override public T bind() { return (T)error; }

    @Override public int bind(IntSupplier success, IntSupplier failure) { return failure.getAsInt(); }
  }

  public static <T> Result<T> success(T value) { return new Success<>(value); }

  public static <T> Result<T> failure(String message) { return new Failure<>(message); }

  public static <T> Result<T> failure(SQLException sqlex) { return new Failure<>(sqlex); }
}

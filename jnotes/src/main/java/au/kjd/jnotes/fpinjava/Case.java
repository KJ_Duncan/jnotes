package au.kjd.jnotes.fpinjava;

import java.sql.SQLException;
import java.util.function.BooleanSupplier;

/* An abstraction over if...else control structures in the cli package.
 * source adapted:
 *   Saumont, P-Y 2017, Listing 3.8 Matching conditions with the Case class, Abstracting control structures,
 *                      Functional Programming in Java, chpt 3, p.69, Manning Publications,
 *                      viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java>
 */
public class Case<T> extends Pair<BooleanSupplier, Supplier<Result<T>>> {

  private Case(BooleanSupplier booleanSupplier, Supplier<Result<T>> resultSupplier) { super(booleanSupplier, resultSupplier); }

  private static class DefaultCase<T> extends Case<T> {

    private DefaultCase(BooleanSupplier booleanSupplier, Supplier<Result<T>> resultSupplier) { super(booleanSupplier, resultSupplier); }
  }

  public static <T> Case<T> cause(BooleanSupplier condition, Supplier<Result<T>> value) { return new Case<>(condition, value); }

  public static <T> DefaultCase<T> cause(Supplier<Result<T>> value) { return new DefaultCase<>(() -> true, value); }

  /* implementation evaluates the default case is last, for understanding on the SQLException see the Supplier interface */
  @SafeVarargs public static <T> Result<T> match(DefaultCase<T> defaultCase, Case<T>... matchers) throws SQLException {

    for (Case<T> aCase : matchers) if (aCase.first.getAsBoolean()) return aCase.second.get();

    return defaultCase.second.get();
  }
}

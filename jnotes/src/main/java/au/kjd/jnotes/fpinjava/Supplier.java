package au.kjd.jnotes.fpinjava;

import java.sql.SQLException;

/* Java methods are strict: a Supplier is lazy.
 * To test the Case.cause the method parameters
 * need to be lazy as the right side would
 * throw a null pointer exception on the strict
 * evaluation of non-user choices.
 * The Supplier needs to throw a SQLException
 * from our sqlite jdbc logic (SQLCommands) or
 * we would need to wrap the lambda call in a
 * verbose try catch clause for each Supplier.
 */
@FunctionalInterface public interface Supplier<R> { R get() throws SQLException; }

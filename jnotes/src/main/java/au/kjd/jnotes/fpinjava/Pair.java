package au.kjd.jnotes.fpinjava;


/* A container to hold
 * <BooleanSupplier, Supplier<Result<T>>>
 * in the Case class */
public class Pair<T, U> {

  public final T first;
  public final U second;

  public Pair(T t, U u) {
    this.first = t;
    this.second = u;
  }
}

package au.kjd.jnotes;

import au.kjd.jnotes.cli.Cli;

import java.lang.InterruptedException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import picocli.CommandLine;

import static java.lang.Integer.MIN_VALUE;
import static java.lang.Runtime.getRuntime;
import static java.util.concurrent.TimeUnit.SECONDS;

/*
 * All commands require a path to a named database, creation of the
 * database will occur if it doesn't exist on the path.
 *   jnotes --file /path/to/named.db
 *
 * Each database has the schema:
 *   CREATE TABLE jnotes (
 *     jno_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
 *     jno_tag  TEXT NOT NULL,
 *     jno_date TEXT NOT NULL,
 *     jno_text TEXT NOT NULL,
 *
 *     CONSTRAINT jno_ck1 CHECK ( 0 < LENGTH( jno_tag  ) ),
 *     CONSTRAINT jno_ck2 CHECK ( 0 < LENGTH( jno_text ) )
 *   );
 *
 * The `jno_date` field in automatically derived with current date in
 * the format YYYY-MM-DD.
 *
 * Four sub-commands are available; insert, update, delete, and select.
 * There individual options are:
 *   insert [-h] [-t=label] -w=text
 *   update [-h] [-t=pk label] [-w=pk text] [-g=label label]
 *   delete [-h] [-p=integer] [-t=label]
 *   select [-h] [-a] [-p=integer] [-t=label] [-d=yyyy-mm-dd]
 *
 * For a long form message use: sub-command --help
 *
 * Due to the number of command line combinations best get some help with cli.
 *
 * Picocli is a lightweight command line builder and parser,
 * using annotations in their api with the use of reflection.
 *
 * Picocli supports GraalVM's ahead-of-time compiler to create a
 * standalone executable also known as a native-image.
 *
 * Picocli 2021, Picocli on GraalVM: Blazingly Fast Command Line Apps, v4.6.1,
 *               viewed 26 September 2021, <https://picocli.info/picocli-on-graalvm.html>
 *
 * Oracle 2021, GraalVM Community Version Roadmap, GraalVM, viewed 03 October 2021,
 *              <https://www.graalvm.org/release-notes/version-roadmap/>
 *
 * Eminem 2001, Stan - Instrumental, The Marshall Mathers LP, viewed 22 September 2021,
 *              <https://open.spotify.com/track/4tME51EiaLJvxGZExXRdRX?si=84f5fde897364ab4>
 */

final class Main {

  private static final Logger logger = Logger.getLogger(Main.class.getName());

  /* options run in its own thread (non-blocking) */
  private static final ExecutorService exec = Executors.newSingleThreadExecutor();

  public static void main(String[] args) {
    if (MIN_VALUE == args.length + 1) getRuntime().exit(1);

    int exitCode = -1;

    try { Future<Integer> future = exec.submit(() -> new CommandLine(new Cli()).execute(args));
          exitCode = future.get(5L, SECONDS); }

    catch (InterruptedException|
           ExecutionException|
           TimeoutException ex) { logger.severe(ex::getLocalizedMessage); }
    finally { getRuntime().exit(exitCode); }
  }
}

package au.kjd.jnotes;

import au.kjd.jnotes.data.model.*;
import au.kjd.jnotes.cli.*;
import au.kjd.jnotes.data.domain.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

import picocli.CommandLine;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

/* ./gradlew :jnotes:test --tests "au.kjd.jnotes.MainTest" */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MainTest {

  @AfterAll static void shutDown() throws IOException {

    Files.deleteIfExists(Paths.get("notes.db").toAbsolutePath());
  }

  // requires: Subcommands <https://picocli.info/#_subcommands>
  @Order(1)
  @Test void testInsertHelp() {

    String[] args = { "--file", "notes.db", "insert", "--help" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));
  }

  @Order(2)
  @Test void testInsertWrite() throws SQLException {

    String[] args = { "--file", "notes.db", "insert", "--write", "it works" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));
    assertThat(SQLCommands.NotesDQL.select(new ID(1)).get(0).toString()).contains("default", "it works");
    SubClassTCL.disconnect();
  }

  @Order(3)
  @Test void testInsertTagWrite() throws SQLException {

    String[] args = { "--file", "notes.db", "insert", "--tag", "hope", "--write", "it works" }; // "--file", "notes.db",

    assertEquals(0, new CommandLine(new Cli()).execute(args));
    assertThat(SQLCommands.NotesDQL.select(new ID(2)).get(0).toString()).contains("hope", "it works");
    SubClassTCL.disconnect();
  }

  @Order(4)
  @Test void testSelectPk() {

    String[] args = { "--file", "notes.db", "select", "--primary-key", "1" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    SubClassTCL.disconnect();
  }

  @Order(5)
  @Test void testSelectTag() {

    String[] args = { "--file", "notes.db", "select", "--tag", "hope" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    SubClassTCL.disconnect();
  }

  @Order(6)
  @Test void testSelectDate() {
    java.time.LocalDate date = java.time.LocalDate.now();

    String[] args = { "--file", "notes.db", "select", "--date", date.toString() };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    SubClassTCL.disconnect();
  }

  @Order(7)
  @Test void testUpdateTag() throws SQLException {

    String[] args = { "--file", "notes.db", "update", "--tag", "1", "thanks" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    assertThat(SQLCommands.NotesDQL.select(new ID(1)).get(0).toString()).contains("thanks", "it works");
    SubClassTCL.disconnect();
  }

  @Order(8)
  @Test void testUpdateWrite() throws SQLException {

    String[] args = { "--file", "notes.db", "update", "--write", "1", "now and then" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    assertThat(SQLCommands.NotesDQL.select(new ID(1)).get(0).toString()).contains("thanks", "now and then");
    SubClassTCL.disconnect();
  }

  @Order(9)
  @Test void testUpdateGroup() throws SQLException {

    String[] args = { "--file", "notes.db", "update", "--group", "hope", "generic" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    assertThat(SQLCommands.NotesDQL.select(new ID(2)).get(0).toString()).contains("generic", "it works");
    SubClassTCL.disconnect();
  }

  @Order(10)
  @Test void testDeletePk() throws SQLException {

    String[] args = { "--file", "notes.db", "delete", "--primary-key", "2" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    assertEquals(1, SQLCommands.NotesDQL.select().size());
    SubClassTCL.disconnect();
  }

  @Order(11)
  @Test void testDeleteTag() throws SQLException {

    String[] args = { "--file", "notes.db", "delete", "--tag", "thanks" };

    assertEquals(0, new CommandLine(new Cli()).execute(args));  // System.exit(0 or 1)
    assertEquals(0, SQLCommands.NotesDQL.select().size());
    SubClassTCL.disconnect();
  }
}

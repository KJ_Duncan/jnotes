package au.kjd.jnotes.data.domain;

import java.util.logging.Logger;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DtoTest {
  private static final Logger logger = Logger.getLogger(DtoTest.class.getName());

  @Test void testJNotesDTO() {
    java.time.LocalDate date = java.time.LocalDate.of(2019, 9, 19);

    JNotesDTO dto = new JNotesDTO(1, "fisher", date.toString(), "you little beauty");

    assertEquals(1, dto.jnoId());
    assertTrue(dto.jnoTag().matches("fisher"));
    assertTrue(dto.jnoDate().matches(date.toString()));
    assertTrue(dto.jnoText().matches("you little beauty"));
    assertThat(dto.toString()).contains("1", "fisher", "2019-09-19", "you little beauty");

    logger.info(dto.toString());
  }
}

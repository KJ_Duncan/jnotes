package au.kjd.jnotes.data.domain;

import au.kjd.jnotes.data.model.DATE;
import au.kjd.jnotes.data.model.ID;
import au.kjd.jnotes.data.model.TAG;
import au.kjd.jnotes.data.model.TEXT;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import java.util.logging.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.function.ThrowingSupplier;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatObject;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

/* ./gradlew :app:test --tests "au.kjd.jnotes.data.domain.SQLCommandsTest" */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SQLCommandsTest {

  private static final Logger logger = Logger.getLogger(SQLCommandsTest.class.getName());

  static Connection conn;

  @AfterAll static void shutDown() throws IOException, SQLException {

    conn.close();

    Files.deleteIfExists(Paths.get("notes.db").toAbsolutePath());
  }

  @Order(1)
  @Test void testDatabaseProcessor() throws SQLException {

    SubClassTCL.init("notes.db");
    conn = SubClassTCL.connect();

    assertFalse(conn.isClosed());
    // jdbc connection type 4 no requirement to load driver.
    assertThatObject(conn).isExactlyInstanceOf(org.sqlite.jdbc4.JDBC4Connection.class);

  }

  @Order(2)
  @Test void testCreateDDL() throws SQLException {

    SubClassTCL.init("notes.db");
    SQLCommands.NotesDDL.create();

    assertDoesNotThrow(() -> SQLCommands.NotesDQL.select());
  }

  @Order(3)
  @Test void testInsertDML() {

    TAG tag = new TAG("test");
    TEXT text = new TEXT("better here than there");

    assertDoesNotThrow(() -> SQLCommands.NotesDML.insert(text));
    assertDoesNotThrow(() -> SQLCommands.NotesDML.insert(tag, text));

    assertThrows(SQLException.class, () -> SQLCommands.NotesDML.insert(new TEXT("")));
    assertThrows(SQLException.class, () -> SQLCommands.NotesDML.insert(new TAG(""), text));
    assertThrows(SQLException.class, () -> SQLCommands.NotesDML.insert(tag, new TEXT("")));
  }

  @Order(4)
  @Test void testSelectDQL() throws SQLException {

    int PK_3 = 3;
    java.time.LocalDate date = java.time.LocalDate.now();

    assertDoesNotThrow(() -> SQLCommands.NotesDML.insert(new TAG("music"), new TEXT("the salmon dance")));

    assertAll("SQL command SELECT does not throw Exception",
        () -> assertDoesNotThrow(() -> SQLCommands.NotesDQL.select(new ID(PK_3))),
        () -> assertDoesNotThrow(() -> SQLCommands.NotesDQL.select(new TAG("music"))),
        () -> assertDoesNotThrow(() -> SQLCommands.NotesDQL.select(new DATE(date))),
        () -> assertDoesNotThrow((ThrowingSupplier<List<Dto>>) SQLCommands.NotesDQL::select));

    List<List<Dto>> datastore = List.of(SQLCommands.NotesDQL.select(new ID(PK_3)),
                                        SQLCommands.NotesDQL.select(new TAG("music")),
                                        SQLCommands.NotesDQL.select(new DATE(date)),
                                        SQLCommands.NotesDQL.select());

    // jno_date=1632039152813 is not yyyy-mm-dd
    assertThat(datastore.get(0).toString())
                        .contains("2")
                        .contains("music")
                        .contains(date.toString())
                        .contains("the salmon dance");

    String prettyprint = datastore.get(0).toString();
    logger.fine(() -> prettyprint.startsWith("[") ? format("%n%s", datastore.get(0).toString().substring(1, prettyprint.length() - 1)) : prettyprint);
  }

  @Order(5)
  @Test void testDeleteDML() throws SQLException {

    ID Id_4 = new ID(4);
    TAG another = new TAG("another");

    SQLCommands.NotesDML.insert(new TAG("other"), new TEXT("test text to be deleted"));

    assertDoesNotThrow(() -> SQLCommands.NotesDML.delete(Id_4));

    SQLCommands.NotesDML.insert(another, new TEXT("test text to be deleted"));
    SQLCommands.NotesDML.insert(another, new TEXT("duplicated test text to be deleted"));

    assertDoesNotThrow(() -> SQLCommands.NotesDML.delete(another));

    assertDoesNotThrow(() -> SQLCommands.NotesDQL.select(Id_4));
    assertDoesNotThrow(() -> SQLCommands.NotesDQL.select(another));

    // assertFalse(conn.isClosed());

    assertThat(SQLCommands.NotesDQL.select().toString()).doesNotContain("other", "another", "test text to be deleted");

    // SQLCommands.NotesDQL.select().forEach(e -> logger.fine(e.toString()));

    /*
    TEST: SQLCommandsTest Notes::Schema,MetaData
    DatabaseMetaData meta = conn.getMetaData();
    var driver = meta.getDriverName();
    var schema = meta.getSchemas();
    var catalog = meta.getCatalogs();
    var client = meta.getMaxConnections();

    logger.fine(format("Driver: %s", driver));
    logger.fine(format("Schema: %s", schema));
    logger.fine(format("Catalog: %s", catalog));
    logger.fine(format("Client: %s", client));
     */
  }

  @Order(6)
  @Test void testUpdateDML() throws SQLException {

    int PK_2 = 2;
    ID id_2 = new ID(PK_2);

    String song = "song";
    TAG tag_song = new TAG(song);

    assertDoesNotThrow(() -> SQLCommands.NotesDQL.select(id_2));
    // set all tags with name 'test' to 'song'
    assertAll("SQL command UPDATE does not throw Exception",
        () -> assertDoesNotThrow(() -> SQLCommands.NotesDML.update(id_2, tag_song)),
        () -> assertDoesNotThrow(() -> SQLCommands.NotesDML.update(id_2, new TEXT("how about no where"))));

    assertAll("SQL command UPDATE equals SELECT statement",
        () -> assertThat(SQLCommands.NotesDQL.select(id_2).get(0).toString()).contains(song),
        () -> assertThat(SQLCommands.NotesDQL.select(tag_song).get(0).toString()).contains("how about no where"));

    assertDoesNotThrow(() -> SQLCommands.NotesDML.update(tag_song, new TAG("test")));
    assertThat(SQLCommands.NotesDQL.select(id_2).get(0).toString()).contains("test");

    logger.fine(SQLCommands.NotesDQL.select(id_2).get(0).toString());
  }

  @Tag("Broken") // SubClassTCL is now encapsulated
  @Order(7)
  @Test void testRollback() {

    /*
    // NOTE: Savepoint, throw an error to rollback from, instantiate savepoint post ps.execute
    private boolean transaction(PreparedStatement ps, Savepoint sp) throws SQLException {
      try { throw new SQLException("test case exception"); }
      catch (SQLException sqle) { rollback().accept(connect(), sp); throw sqle; }
      finally { ps.clearParameters(); }
    }

    SubClassTCL.init("notes.db");

    SubClassDDL.init();

    Supplier<Savepoint> sp = () -> SubClassTCL.savepoint().apply(conn);

    assertDoesNotThrow(sp::get);
    assertDoesNotThrow(() -> tcl.rollback().accept(conn, sp.get()));
     */
  }
}


package au.kjd.jnotes.data.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DataDomainTest {

  @Test void testJNotesAttributes() {
    assertEquals("jno_id", JNotes.JNO_ID.getAttribute());
    assertEquals("jno_tag", JNotes.JNO_TAG.getAttribute());
    assertEquals("jno_date", JNotes.JNO_DATE.getAttribute());
    assertEquals("jno_text", JNotes.JNO_TEXT.getAttribute());
  }

  @Test void testJNotesOrdinals() {
    assertEquals(16, JNotes.values().length);
    assertEquals(0, JNotes.values()[0].ordinal());
    assertEquals(1, JNotes.values()[1].ordinal());
    assertEquals(15, JNotes.values()[15].ordinal());

    assertThrows(IndexOutOfBoundsException.class, () -> JNotes.values()[16].ordinal());
  }
}

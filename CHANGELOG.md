# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0 - 2021-10-26

- minor: improved encapsulation SQLCommands to an abstract class
- patch: disconnects from jdbc prior to jvm shutdown

## 0.1.2 - 2021-10-21

- patch: graalvm 21.3.0 build with command line support

## 0.1.1 - 2021-10-16

- patch: Data flow modelling: SubClassTCL now maintains the database connection

## 0.1.0 - 2021-10-10

- minor: Saumont, P-Y 2017, How functional techniques improve your Java programs

## 0.0.3 - 2021-10-08

- patch: SQLCommands commits to savepoint after a rollback or throws exception

## 0.0.2 - 2021-10-08

- patch: parent Cli.java handles file path, all subcommands inherit database path

## 0.0.1 - 2021-10-08

- patch: improved help usage message for jnotes-update



A note-taking command-line application to sqlite database with Kakoune editor integration.  


![](plantuml/plantuml-jnotes.png)


#### Usage


All commands require a path to a named database, on insert command creation of the database will occur if it doesn't exist on the path.
```text
Usage: jnotes [-h] -f=path [COMMAND]
  -f, --file=path   path to local sqlite database
  -h, --help        display this help message
Commands:
  delete  sql data manipulation language on an sqlite database.
  insert  sql data manipulation language on an sqlite database.
  select  sql data query language on an sqlite database.
  update  sql data manipulation language on an sqlite database.
  
#----------------------------------------------------------------------------------------------------#
$ jnotes-insert --help
Usage: jnotes insert [-h] [-t=label] -w=text
sql data manipulation language on an sqlite database.
  -h, --help         display this help message
  -t, --tag=label    insert a tag label to group by into table
  -w, --write=text   insert written "text requires quoting" into table

#----------------------------------------------------------------------------------------------------#
$ jnotes-select --help
Usage: jnotes select [-h] [-a] [-p=integer] [-t=label] [-d=yyyy-mm-dd]
sql data query language on an sqlite database.
  -a, --all               select all rows from jnotes
  -d, --date=yyyy-mm-dd   select rows by date
  -h, --help              display this help message
  -p, --primary-key=integer
                          select row by primary key
  -t, --tag=label         select rows by tag

#----------------------------------------------------------------------------------------------------#
$ jnotes-update --help
Usage: jnotes update [-h] [-t=pk label] [-w=pk text] [-g=label label]
sql data manipulation language on an sqlite database.
  -g, --group=label label   update the tag attribute rows group by the --group <before> <after>
  -h, --help                display this help message
  -t, --tag=pk label        update a rows tag by its primary key: --tag 1 label
  -w, --write=pk text       update a rows text by its primary key: --write 1 "some text string"

#----------------------------------------------------------------------------------------------------#
$ jnotes-delete --help
Usage: jnotes delete [-h] [-p=integer] [-t=label]
sql data manipulation language on an sqlite database.
  -h, --help        display this help message
  -p, --primary-key=integer
                    delete row by its primary key
  -t, --tag=label   delete rows by the tag attribute
```


----


If you require multiple databases for your notes use the `--file` flag to stipulate the correct one.  
`$ jnotes --file /path/to/music.db insert --tag classical --write "Divertimento in B-Flat Major, K.254: 1. Allegro assai"`
  
Or create them as an executable. You can call an executable in this form from a sub-shell.  
```shell
$ echo 'jnotes --file /path/to/named.db insert "$@"' > jnotes-insert
$ echo 'jnotes --file /path/to/named.db update "$@"' > jnotes-update
$ echo 'jnotes --file /path/to/named.db delete "$@"' > jnotes-delete
$ echo 'jnotes --file /path/to/named.db select "$@"' > jnotes-select

$ chmod u+x,go-rwx jnotes-insert
$ mv jnotes-insert $HOME/local/bin
[...]

$ jnotes-insert --tag song --write "You Little Beauty"
```


#### GraalVM native-image


```shell
# Set up a GRAALVM_HOME environment variable pointing to your GraalVM installation
# Version info: 'GraalVM 22.2.0 Java 17 CE'

# Build the project
$ ./gradlew jnotes:nativeCompile
$ du -sh jnotes/build/native/nativeCompile/jnotes
> 41M    jnotes
$ mv jnotes/build/native/nativeCompile/jnotes $HOME/local/bin

# Run the test suite
$ ./gradlew jnotes:test --rerun-tasks
```
  
Your database is completely accessible from the `sqlite` command line.  
```sql
-- $ sqlite3 named.db
-- sqlite> .schema jnotes
CREATE TABLE jnotes (
  jno_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  jno_tag  TEXT NOT NULL,
  jno_date TEXT NOT NULL,
  jno_text TEXT NOT NULL,

  CONSTRAINT jno_ck1 CHECK ( 0 < LENGTH( jno_tag  ) ),
  CONSTRAINT jno_ck2 CHECK ( 0 < LENGTH( jno_text ) )
);
```


#### Done


- [x] gradle.build.kts builds the GraalVM native-image
- [x] run from any location and find the sqlite.db
- [x] Parent Cli `--file ...` to subcommands <https://picocli.info/#_subcommands>
- [x] Kakoune integration see file `jnotes.kak`


#### Readings


- Saumont, P-Y 2017, Functional Programming in Java, Manning Publications, viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java> 
- Oracle 2021, _'Native Image'_, GraalVM, v21.3, viewed 21 October 2021, <https://www.graalvm.org/reference-manual/native-image/>
- Picocli 2021, _'29.3. GraalVM Native Image'_, v4.6.1, viewed 26 September 2021, <https://picocli.info/#_graalvm_native_image>
- Picocli 2021, _'Picocli on GraalVM: Blazingly Fast Command Line Apps'_, v4.6.1, viewed 26 September 2021, <https://picocli.info/picocli-on-graalvm.html>
- Sqlite 2021, _'SQLite'_, v3.36.0, viewed 26 September 2021, <https://sqlite.org/index.html>
- Xerial 2021, _'SQLite JDBC Driver'_, v3.36.0.3, viewed 26 September 2021, <https://github.com/xerial/sqlite-jdbc>
- Sonatype 2022, _'info.picocli:picocli:4.6.3'_, Maven Central Search Repository, viewed 29 August 2022, <https://search.maven.org/artifact/info.picocli/picocli/4.6.3/jar>
- Sonatype 2022, _'org.xerial:sqlite-jdbc:3.39.2.0'_, Maven Central Search Repository, viewed 29 August 2022, <https://search.maven.org/artifact/org.xerial/sqlite-jdbc/3.39.2.0/jar>


#### jlink


```shell
$ ./gradlew jlink

$ cd jnotes/build/
$ du -sh jrt-jnotes
> 59M    jrt-jnotes

$ jrt-jnotes/bin/java --module-path sqlite-jdbc-3.36.0.3.jar \
                      --module au.kjd.jnotes/au.kjd.jnotes.Main \
                      --file music.db insert --tag fisher --write "Ya Didn't"
```


#### jpackage


```shell
# here we go 
$ ./gradlew jar
$ cd jnotes/build/libs/

$ mv /path/to/picocli-4.6.3.jar .
$ mv /path/to/sqlite-jdbc-3.39.2.0.jar .

# error: automatic module cannot be used with jlink: org.xerial.sqlitejdbc
# we need to modularise the `org.xerial.sqlitejdbc`.
$ jdeps -verbose --generate-module-info . sqlite-jdbc-3.39.2.0.jar
$ javac --module-path sqlite-jdbc-3.39.2.0.jar --patch-module org.xerial.sqlitejdbc=sqlite-jdbc-3.39.2.0.jar org.xerial.sqlitejdbc/module-info.java
# Move the module descripter to the root folder otherwise updating the jar won't work properly (ref: Parlog, N 2019)
$ mv org.xerial.sqlitejdbc/module-info.class .
$ jar --update --file sqlite-jdbc-3.39.2.0.jar module-info.class

$ jar --describe-module --file sqlite-jdbc-3.39.2.0.jar
> org.xerial.sqlitejdbc jar:file:///.../jnotes/jnotes/build/libs/sqlite-jdbc-3.39.2.0.jar!/module-info.class
> exports org.sqlite
> exports org.sqlite.core
> exports org.sqlite.date
> exports org.sqlite.javax
> exports org.sqlite.jdbc3
> exports org.sqlite.jdbc4
> exports org.sqlite.util
> requires java.base mandated
> requires java.logging transitive
> requires java.sql transitive
> provides java.sql.Driver with org.sqlite.JDBC

$ jlink --strip-native-commands \
        --strip-debug \
        --no-man-pages \
        --no-header-files \
        --compress 2 \
        --output jrt-jnotes \
        --module-path jnotes-0.2.0.jar:sqlite-jdbc-3.39.2.0.jar:picocli-4.6.3.jar:$JAVA_HOME/jmods \
        --add-modules au.kjd.jnotes,org.xerial.sqlitejdbc,info.picocli,java.base,java.logging,java.sql,java.transaction.xa,java.xml

$ jpackage --verbose \
           --type app-image \
           --name jnotes-app \
           --module au.kjd.jnotes/au.kjd.jnotes.Main \
           --runtime-image jrt-jnotes \
           --java-options -XX:+UseSerialGC

$ chmod -R go-rwx jnotes-app
$ du -sh jnotes-app
> 70M    jnotes-app

$ jnotes-app/bin/jnotes-app --file music.db insert --tag fisher --write "Just Feels Tight"
$ jnotes-app/bin/jnotes-app --file music.db select --tag fisher
> _______________________________________________________________________________
> 3 | fisher | 2021-10-14 | Ya Didnt
> _______________________________________________________________________________
> 4 | fisher | 2021-10-14 | Just Feels Tight

$ echo '$HOME/path/to/jnotes-app/bin/jnotes-app --file /path/to/named.db insert "$@"' > jnotes-insert
[...]
$ chmod u+x,go-rwx jnotes-insert
$ mv jnotes-insert $HOME/local/bin/

# and your good-to-go or continue with jpackage to create a platform package of your choice: "exe", "msi", "rpm", "deb", "pkg", "dmg"
```


- Oracle 2021, _'The jpackage Command'_, Java Development Kit Version 17 Tool Specifications, All Platforms, viewed 14 September 2021, https://docs.oracle.com/en/java/javase/17/docs/specs/man/jpackage.html


#### Lessons learnt


The 'Commons CLI' library does not support the java module system. Manual support can be provided through the 'MANIFEST.MF' Automatic-Module-Name declaration or via the command line:  

```shell
# reads the MANIFEST.MF file to stdout
$ unzip -p commons-cli-1.4.jar META-INF/MANIFEST.MF

$ mkdir jars
$ mv commons-cli-1.4.jar jars
$ jdeps -verbose --generate-module-info . jars
$ javac --module-path jars --patch-module commons.cli=jars/commons-cli-1.4.jar commons.cli/module-info.java
$ mv commons.cli/module-info.class .
$ jar --update --file jars/commons-cli-1.4.jar module-info.class
$ jar --describe-module --file jars/commons-cli-1.4.jar
```

As a result the SHA256 checksum is altered.  

- Apache 2017, _'Apache Commons CLI'_, v1.4, viewed 26 September 2021, <https://commons.apache.org/proper/commons-cli/index.html>
- Parlog, N 2019, _'9.3.3 Hacking third-party JARs'_, Making JARs modular, The Java Module System, pp.210-211, viewed 26 September 2021, <https://www.manning.com/books/the-java-module-system>


